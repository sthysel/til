# Tagging

```
git tag -a v1.4 -m "my version 1.4"
git push --tags
```
-a add a new tag

Tags need to be explicitly pushed.

